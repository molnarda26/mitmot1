#include "mcu_avr_atmega128_api.h" 	// MCU API   
#include "dpy_trm_s01.h"			// DPY API

#define ROUNDS 10
#define GAMES 10


ISR(SIG_OVERFLOW0);
int main(void);
void Timer0_Init(int);
float RecursiveAvg(int[], int , int );
void display_select(void);

unsigned char seg_que[7] = {~(0x01<<0),~(0x01<<1),~(0x01<<2),~(0x01<<7),
							~(0x01<<6),~(0x01<<5),~(0x01<<4)};
							//cnt==5-n�l van a c�l szegmens kint
unsigned char led=0x1;
volatile unsigned char cnt=0xFF;
volatile unsigned char display = 0;
unsigned char but1, but2, but3;
unsigned int speed = 9374;
int score=0;
int score_max=0;
int score_min=0xFF;
int game_cnt=0;
int score_array[10];
unsigned char round_cnt = 0;
unsigned char but_pushed;
unsigned char sw1,sw2,sw3,sw4;



int main (void)
{
   dpy_trm_s01__Init();		// Initialize the DPY dysplay card
   SYS_LED_DIR_OUTPUT();	// Set the pin driving the system led to output
   SYS_LED_ON();			// Switch on system led
   Timer0_Init(speed);			// Initialize timer0
   cnt=0xFF;
   while(1) 
   {			
		if(round_cnt<ROUNDS+1) //legyen vmi define konstans h h�ny k�r legyen �sszesen mondjuk
		{
			if(game_cnt>GAMES) game_cnt=0;
			
			but2=DPY_TRM_S01__BUTTON_2_GET_STATE(); //waiting for hit
			if (!but2) 
			{
			
				TIMSK&=~(1<<OCIE1A); //stop timer
				TCCR1B=0x00;
				but2=~but2;			 //'disable' button...? a k�vetkez� nyom�s miatt

				if(cnt==5)
				{
		    		DPY_TRM_S01__LED_4_ON(); //if score
					score+=15;
				}
				else 
				{
					DPY_TRM_S01__LED_1_ON();//else
					if(cnt<5)
						score+=11-(5-cnt); //el�tte nyomtunk
					else
						score+=11-1; //ez amikor ut�na nyomtunk
				}

				_delay_ms(150); //debounce...?
				while(but2) //waiting for button again
				{	//do stuff here
					if(!but3){ speed+=250; } //k�s�bb comparel -> lassabb
					else if(!but1){ speed-=250;} //el�bb comparel -> gyorsabb
					//---------------
					but1=DPY_TRM_S01__BUTTON_1_GET_STATE();
					but3=DPY_TRM_S01__BUTTON_3_GET_STATE();
					but2=DPY_TRM_S01__BUTTON_2_GET_STATE();
					_delay_ms(150);
				}
				 //debounce...?
				Timer0_Init(speed);
				DPY_TRM_S01__LED_1_OFF();
				DPY_TRM_S01__LED_4_OFF();
				} //if end
		}
		else //game over
		{	
			TIMSK&=~(1<<OCIE1A); //stop timer
			TCCR1B=0x00;
			

			if(score>score_max) score_max=score;
			if(score<score_min) score_min=score;
			if(game_cnt<GAMES) score_array[game_cnt]=score;
			//ezt itt valahogy folyamatosan k�ne n�zni meg v�ltoztatni
			
			
			but2=1;									
									
			while(but2) //waiting for restart
			{	
				display_select();
				but2=DPY_TRM_S01__BUTTON_2_GET_STATE();
				_delay_ms(150);
			}
			score=0;		
			round_cnt=0;
			//but2=~but2;
			//restarting
			Timer0_Init(speed);
			game_cnt++;
		}
   }// while end

}

void Timer0_Init(speed)
{
   cnt=0xFF;
   TCCR1B |= (1 << WGM12); // Configure timer 1 for CTC mode 
   TIMSK |= (1 << OCIE1A); // Enable CTC interrupt 
   sei();					// enable interrupts
   TCCR1B|= 1<<CS12;			// Set TIMER0 prescaler to CLK/256  
   OCR1A = speed;
   cnt=0xFF;                 
}

ISR(TIMER1_COMPA_vect) 
{
	if(cnt>=6)
	{
		cnt=0;
		round_cnt++;
	}
	else cnt++;
	
	dpy_trm_s01__7seq_write_3digit(~0x00,seg_que[cnt],~0x00);
	if(cnt==6) score-=5;
	
}

float RecursiveAvg(int A[], int i, int n)  
{  
    //Base case  
    if (i == n-1) return A[i]/n;  
  
    return A[i]/n + RecursiveAvg(A, i + 1, n);  
}

void display_select(void)
{
	sw1 = DPY_TRM_S01__SWITCH_1_GET_STATE();
	sw2 = DPY_TRM_S01__SWITCH_2_GET_STATE();
	sw3 = DPY_TRM_S01__SWITCH_3_GET_STATE();
	sw4 = DPY_TRM_S01__SWITCH_4_GET_STATE();
	if(sw1 && !sw2 && !sw3 && !sw4) dpy_trm_s01__7seq_write_number(score,0);
	else if(!sw1 && sw2 && !sw3 && !sw4) dpy_trm_s01__7seq_write_number(score_min,0);
	else if(!sw1 && !sw2 && sw3 && !sw4) dpy_trm_s01__7seq_write_number(score_max,0);
	else if(!sw1 && !sw2 && !sw3 && sw4) dpy_trm_s01__7seq_write_number(RecursiveAvg(score_array,0,game_cnt+1),0);
	else dpy_trm_s01__7seq_write_number(score,0);
}
